from django.contrib import admin
from assets.models import Machine, Network
# Register your models here.


admin.site.register(Machine)
admin.site.register(Network)