from rest_framework import routers
from assets.viewsets import MachineViewset
from django.urls import path, include

router = routers.DefaultRouter()
router.register('machines', MachineViewset)


urlpatterns = [
    path('', include(router.urls))
]