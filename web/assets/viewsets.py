import rest_framework.viewsets as viewsets
from assets.serializers import MachineSerializer
from assets.models import Machine


class MachineViewset(viewsets.ModelViewSet):
    queryset = Machine.objects.all()
    serializer_class = MachineSerializer



