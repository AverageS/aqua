from django.db import models
from django.contrib.postgres.fields import ArrayField
import random

# Create your models here.

def create_name():
    with open('tolkien_names.txt') as fp:
        names = [x.strip() for x in fp.readlines()]
    first_name = random.choice(names)
    return first_name


class Network(models.Model):
    name = models.CharField(max_length=256, unique=True, default=create_name)

    def __str__(self):
        return self.name

class Machine(models.Model):
    name = models.CharField(max_length=256)
    os = models.CharField(max_length=64, blank=True, null=True)
    server_type = models.CharField(max_length=128, blank=True, null=True)

    ip = models.GenericIPAddressField()
    ports = ArrayField(models.PositiveIntegerField(), null=True, blank=True)
    network = models.ForeignKey('assets.Network', on_delete=models.CASCADE, null=True)

    mac = models.CharField(max_length=64, blank=True, null=True)
    is_online = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(null=True)

    class Meta:
        unique_together = ['network', 'ip']

    def __str__(self):
        return self.name



