from django.db import models
import random

# Create your models here.

def create_name():
    with open('music_names.txt') as fp:
        names = [x.strip() for x in fp.readlines()]
    first_name = random.choice(names)
    return first_name


class Agent(models.Model):
    name = models.CharField(max_length=128, default=create_name)
    network = models.ForeignKey('assets.Network', on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class Scan(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE)
    machines = models.ManyToManyField('assets.Machine')