import rest_framework.serializers as serializers
import agents.models as models


class AgentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Agent
        fields = '__all__'

class ScanSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Scan
        fields = '__all__'
        depth = 2