import datetime

import rest_framework.viewsets as viewsets
from rest_framework.decorators import action
from django.http import JsonResponse
import agents.serializers as serializers
import agents.models as agent_models
import assets.models as asset_models


class AgentViewset(viewsets.ModelViewSet):
    queryset = agent_models.Agent.objects.all()
    serializer_class = serializers.AgentSerializer

    @action(detail=False, methods=['POST'])
    def register(self, request):
        network_name = request.data.get('network', None)
        if network_name is None:
            network = asset_models.Network.objects.create()
        else:
            try:
                network = asset_models.Network.objects.get(name=network_name)
            except asset_models.Network.DoesNotExist:
                return JsonResponse({},status=400)
        agent = agent_models.Agent.objects.create(network=network)
        agent_serialized = serializers.AgentSerializer(agent).data
        return JsonResponse(agent_serialized)


    @action(detail=True, methods=['POST'])
    def scan_info(self, request, pk):
        agent = self.get_object()
        data = request.data
        network = agent.network
        Machine = asset_models.Machine
        scan = agent_models.Scan.objects.create(agent=agent)
        for vm in data:
            name = vm['name']
            ip = vm['ip']
            ports = vm['ports']
            machine, created = Machine.objects.get_or_create(name=name, network=network, ip=ip)
            scan.machines.add(machine)
            if not created:
                machine.last_update = datetime.datetime.now()
            machine.ports = ports
            machine.save()

        return JsonResponse({}, status=200)


class ScanViewset(viewsets.ReadOnlyModelViewSet):
    queryset = agent_models.Scan.objects.all()
    serializer_class = serializers.ScanSerializer

