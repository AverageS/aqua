from rest_framework import routers
from agents.viewsets import AgentViewset, ScanViewset
from django.urls import path, include

router = routers.DefaultRouter()
router.register('agents', AgentViewset)
router.register('scans', ScanViewset)


urlpatterns = [
    path('', include(router.urls))
]