# Generated by Django 4.1 on 2023-11-14 12:36

import agents.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agents', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agent',
            name='name',
            field=models.CharField(default=agents.models.create_name, max_length=128),
        ),
    ]
