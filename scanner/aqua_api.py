import requests

class Aqua():
    def __init__(self, base_url ,token, network=None):
        self.token = token
        self.headers = {
            "Authorization": f"Token {token}"
        }
        self.base_url = base_url

        r = requests.post(f"{self.base_url}agents/agents/register/",headers=self.headers, json={
            "network": network
        })
        data = r.json()
        self.name = data['name']
        self.server_id = data['id']
        self.network = data['network']


    def create_host(self, host, ports):
        r = requests.post(f"{self.base_url}assets/machines/", json={
            "name": host,
            "ips": [host],
            "ports": ports
        })
        print(r.json())

    def send_scan_results(self, scan_results):
        r = requests.post(f"{self.base_url}agents/agents/{self.server_id}/scan_info/",
                          headers=self.headers,
                          json=scan_results)
        print(r.json())