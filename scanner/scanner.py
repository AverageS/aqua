import time
import os

import nmap
import csv
from collections import defaultdict
from aqua_api import Aqua

BASE_URL = os.getenv("BASE_URL", "http://127.0.0.1:8000/api/v1/")
TOKEN = os.getenv("TOKEN", "5dafe61ac304b608b02d15bba9401d758fbd01e3")
SUBNET = os.getenv('SUBNET', "192.168.1.1/28")
NETWORK = os.getenv("NETWORK_NAME", None)

nmScan = nmap.PortScanner()
api = Aqua(BASE_URL, TOKEN)

while True:
    print(f"STARTED SCAN {SUBNET}")
    nmScan.scan(hosts=SUBNET)
    comma_separated = nmScan.csv().splitlines()
    reader = csv.DictReader(comma_separated, delimiter=';')
    all_data = list(reader)
    hosts = defaultdict(list)
    for port in all_data:
        hosts[port['host']].append(port['port'])
    scan_results = []

    for host, ports in hosts.items():
        scan_results.append({
            "name": host,
            "ip": host,
            "ports": ports
        })
    print(f"FINISHED SCAN SENDING RESULT")
    api.send_scan_results(scan_results)
    print(all_data)
    print("GOING TO SLEEP 3600 secs")
    time.sleep(3600)